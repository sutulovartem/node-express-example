const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    firstName: String,
    lastName: String,
    email: String,
    phone: String,
    address: {
        streetAddress: String,
        city: String,
        state: String,
        zip: String
    },
    description: String
});

const User = mongoose.model("User", UserSchema);
module.exports = User;
